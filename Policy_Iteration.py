import numpy as np
import pandas as pd
from maze_env import MAZE_H, MAZE_W


class PolicyIteration:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, theta=0.0001):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.theta = theta
        self.v_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name = "Policy Iteration"


    def policy_evaluation(self, policy):
        # start with all 0 value function
        state_num = MAZE_H * MAZE_W
        V = np.zeros(state_num)
        delta = float("inf")

        while delta > self.theta:
            delta = 0
            # for each state, perform a full backup
            for s in range(state_num):
                v = 0
                # Look at the next possible actions
                for action, action_prob in enumerate(policy[s]):
                    # for each action, look at the possible next states
                    for prob, s_, r, done in

    def policy_improvement(self, s, a, r, s_):

    def
