# import numpy as np
# import pandas as pd
#
# class ValueIteration:
#     def __init__(self, actions, env, learning_rate=0.01, reward_decay=0.9, theta=0.0001):
#         self.actions = actions
#         self.env = env
#         self.lr = learning_rate
#         self.gamma = reward_decay
#         self.theta = theta
#         self.nS = env.MAZE_H * env.MAZE_W
#         self.V = pd.DataFrame(columns=self.actions, dtype=np.float64)
#         self.display_name = "Value Iteration"
#
#     def step(self, action):
#
#     def next_best_action(self, s, V):
#         action_values = np.zeros(self.env.n_actions)
#         for a in range(self.env.n_actions):
#             s_, reward, done = self.env.step(a)
#             action_values[a] += reward + self.gamma * V.loc[str(s_), :]
#         return np.argmax(action_values), np.max(action_values)
#
#     def optimize(self):
#         delta = float("inf")
#         while delta > self.theta:
#             delta = 0
#             for s in range(self.nS):
#                 best_action, best_action_Value = self.next_best_action(s, self.V)
#                 delta = max(delta, np.abs(best_action_Value - self.V[s]))
#                 self.V[s] = best_action_Value
#         policy = np.zeros(self.nS)
#         for s in range(self.nS):
#             best_action, best_action_Value = self.next_best_action(s, self.V)
#             policy[s] = best_action
#
#         return policy


import numpy as np
import pandas as pd


class ValueIteration:

    def __init__(self, actions, reward_decay=0.9, theta=0.001):
        self.actions = actions
        self.gamma = reward_decay
        self.theta = theta
        self.display_name = "Value Iteration"

    def main(self, States = 100, Rewards = 0):
        V = {}
        Policy = {}
        for s in States:
            V[s] = 0
            Policy[s] = [0, 1, 2, 3]

        while True:
            for s in States:
                v = V[s]
                delta = 0
                for a in Policy[s]:
                    s_ = s
                    if (a == 0):
                        s_[1] = s[1] - 40
                        s_[3] = s[3] - 40
                    elif (a == 1):
                        s_[1] = s[1] + 40
                        s_[3] = s[3] + 40
                    elif (a == 2):
                        s_[0] = s[0] + 40
                        s_[2] = s[2] + 40
                    else:
                        s_[0] = s[0] - 40
                        s_[2] = s[2] - 40
                    self.check_state_exist(s_)
                    target = Rewards + (self.gamma * V[s_])
                    V[s] = V[s] + target
                delta = max(delta, np.abs(v - V[s]))
            if (delta < self.theta):
                break

        for s in States:
            optimal_action = None
            optimal_value = float('-inf')
            for a in Policy[s]:
                s_ = s
                if (a == 0):
                    s_[1] = s[1] - 40
                    s_[3] = s[3] - 40
                elif (a == 1):
                    s_[1] = s[1] + 40
                    s_[3] = s[3] + 40
                elif (a == 2):
                    s_[0] = s[0] + 40
                    s_[2] = s[2] + 40
                else:
                    s_[0] = s[0] - 40
                    s_[2] = s[2] - 40
                self.check_state_exist(s_)
                target = Rewards + (self.gamma * V[s_])
                if (target > optimal_value):
                    optimal_value = target
                    optimal_action = a
            Policy[s] = optimal_action

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0] * len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )