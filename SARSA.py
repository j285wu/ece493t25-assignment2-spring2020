import numpy as np
import pandas as pd

'''
Initialize Q(s,a) for all s and a to 0
Repeat (for each episode):
    Initialize S
    Choose A from S using policy derived from Q
    Repeat (for each step of episode):
        Take action A, observe R, S'
        Choose A' from S' using policy derived from Q
        Q(S,A) <- Q(S,A) + alpha[R + gammma * Q(S', A') - Q(S, A)]
        S <- S' A <- A'
    until S is terminal

'''


class SARSA:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name = "SARSA"

    def choose_action(self, observation):
        # implement this.
        self.check_state_exist(observation)

        # if random.uniform >= epsilon -> choose argmax action; else choose random action
        if np.random.uniform() >= self.epsilon:
            state_action = self.q_table.loc[observation, :]
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            action = np.random.choice(self.actions)
        return action



    # Q(S,A) <- Q(S,A) + alpha[R + gammma * Q(S', A') - Q(S, A)]
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            q_target = r + self.gamma * self.q_table.loc[s_, a_]
        else:
            q_target = r  # next state is terminal
        self.q_table.loc[s, a] = self.q_table.loc[s, a] + self.lr * (q_target - self.q_table.loc[s, a])  # update

        return s_, a_


    def check_state_exist(self, state):
        if state not in self.q_table.index:
            self.q_table = self.q_table.append(
                pd.Series(
                    [0] * len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
